 #Stage 1: build
FROM node:16 as build

WORKDIR /app

COPY . .

RUN npm install --include=dev && npm run build

#Stage 2: start
FROM node:16-alpine

WORKDIR /app

COPY --from=build /app .

EXPOSE 8080

CMD ["npm", "start"]
